Systems
=====================================
- [Tickets](https://zillians.atlassian.net/browse/THOR) (JIRA)
- [Continous Integration](https://office.zillians.com/browse/THOR) (Self-hosted Bamboo)
- [VCS](https://bitbucket.org/zillians/thor) (Bitbucket)

## Roles
1. **User**: Anyone with an account to create ticket on Thor ticket system
2. **Developer**: A Thor developer
3. **Owner**: A **Developer** responsible for handling the ticket. Could be assigned or self-assigned
4. **Buildbot**: Continue Integration system
5. **Reviewer**: A **Developer** who reviews solution proposal

Prerequisites
=====================================
- **User** must have an account on Thor's ticket system (JIRA)
- **Developer**
    - Must have accounts in all three systems mentioned above
    - Accounts must use the same (default) email across these systems
    - Accounts must use the same email as his accounts for git commits and pushes

## States Transition Diagram
![alt text](https://bitbucket.org/zillians/thor-workflow/raw/master/thor-workflow.svg "Thor Workflow")

To generate this diagram with graphviz in a shell:
```Shell
$ dot -T svg thor-workflow.dot -o thor-workflow.svg
```

## Workflow
1. A newly created ticket is in **Open** state. The ticket is to be assigned to an **Owner**, either at the time of creation or at a later time. Note that a ticket must have an **Owner** so it could be worked on. Feel free to assign issues to yourself :-)
1. The ticket transits to **Accepted** state when it is accepted by the assigned **Owner**. This **Accepted** state is a clear signal that the assigned **Owner** is aware of the ticket and is (potentially) working towards a solution
    1. **Owner** may **re-assign / reject** the ticket for one of the following reasons:
        1. The ticket was incorrectly assigned. Current **Owner** is not the right person for handling the ticket
        1. The ticket reqiures more information (log, version, evironment setting and etc) to be processed
    1. The ticket is invalid or a duplicate
    1. **Owner** readies a solution
        1. If the solution is on a branch and follows Thor's CI rules (*), it would trigger a build on **buildbot** and transit the ticket to **Building**
            1. When the build is completed, the system sends notification emails to ticket **Owner** and updates the ticket. If the **build passes**, the ticket is transitioned to **Build Passed** state. If the **build fails**, the ticket is transitioned to **Build Broken** state.
            1. Once a ticket is in **Build Passed** state, the **owner** then sends a PR and **request review** on the ticket
        1. If the solution is not on a branch, or if going through the **Builds** sub-workflow path isn't desired, the **owner** should update the ticket with reference(s) to the solution in the ticket before **request review** on the ticket

## Current Implementation and Limitation
1. The **Builds** sub-workflow currently doesn't work as intented. [Self-hosted Bamboo couldn't trigger JIRA ticket state transition](https://confluence.atlassian.com/display/AOD/Integrating+builds+with+your+issues+workflow), which defeats the purpose of having the sub-workflow. As the system is right now, it would be easier for the **owner** to wait for build to pass then **reqeust review** directly
1. To **request review** using JIRA. Go to the ticket -> "More" -> "Watchers", and add reivewers as watchers before click on **request review**
**buildbot** and transit the ticket to **Building**

## Linking Branches/Commits with Tickets
1. Branches
    - Create a branch with the ticket id in the branch name
    - For example: If you want to link a new branch to the ticket **THOR-17**, name the branch ``something/related/to-ticket-THOR-17``
2. Commits
    - Mention the ticket id in commit message
    - For example: If you want to link a commit to the ticket **THOR-17**, ``git commit -n "Something about THOR-17"``
